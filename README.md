# Alguns dos motivos pela qual eu seria um bom programador
* [Tenho um bom raciocínio lógico](#tenho-um-bom-raciocínio-lógico)
* [Sou autodidata](#sou-autodidata)
* [Sei inglês](#sei-inglês)
* [Gosto de aprender coisas novas](#gosto-de-aprender-coisas-novas)
* [Sou bom em resolver problemas](#sou-bom-em-resolver-problemas)
* [Sou naturalmente curioso e proativo](#sou-naturalmente-curioso-e-proativo)
* [Conclusão](#conclusão)


## Tenho um bom raciocínio lógico

Sei que trabalhar com códigos, a princípio, parece assustador para a maioria, mas se analisar os códigos com calma, consigo enxergar padrões e saber mais ou menos o que cada linha do código faz.  
Sei que não basta isso porém, sem isso, ninguém jamais será um bom programador.  
Boa parte do meu conhecimento devo justamente a meu raciocínio lógico.  


## Sou autodidata

Os cursos que completei (com diploma e tudo mais) podem ser contados nos dedos: Datilografia (na máquina de escrever em 1993) Corel Draw 5.0 (acho que foi em 1997) e inglês básico (também em 1997). Porém, para o meu trabalho atual eu uso apenas o inglês, que vou citar no próximo tópico... Todo o resto que eu faço diariamente é fruto de estudo por conta e muita prática.  


## Sei inglês

Mesmo tendo feito apenas o curso de inglês básico de 6 meses no CNA, já estudo inglês por conta desde que tinha 13 anos (hoje em dia entendo praticamente tudo o que leio ou escuto). Aprendi escutando música, assistindo filmes, jogando video game e, olha só que coincidência: Estudando informática (que pelo fato de que computadores pessoais serem demasiado caros na época, acabei começando os estudos nessa área alguns anos mais tarde)...  
Linguagem de códigos basicamente é escrever na língua de computador, mas isso não está exatamente certo (a não ser que se programe em alguma linguagem de baixo nível, o que hoje em dia é raro)...  
As linguagens de alto nível que são amigáveis à linguagem humana... E essa língua é justamente o inglês... Pode pegar qualquer código fonte e analisar superficialmente para constatar isso e, logo, entender o que cada um faz.  


## Gosto de aprender coisas novas

Como em nossa área, de muito pouco ou nada adianta cursar e não se manter atualizado e por em prática o conhecimento, em muito pouco tempo o conhecimento atual torna-se obsoleto. Por esse motivo, é preciso estar sempre disposto a aprender.  
Já fiz quase todos os cursos da [Codecademy](https://www.codecademy.com/pt-BR/infomarck#completed), mas nunca coloquei em prática e hoje já estão obsoletos. Um exemplo é que quando estudei Java Script, o curso era de demasiado extenso, mas hoje só ensinam o essencial, deixando mais detalhes para Frameworks (Angular.js, React.js, etc) que são mais fáceis e mais usados.  


## Sou bom em resolver problemas

De nada adianta desenvolver qualquer coisa se não estiver preparado ou souber como resolver possíveis (e inevitáveis) problemas.  
No meu cargo atual, preciso resolver diversos problemas todos os dias, mas isso não é porque alguma coisa foi mal feita por alguém, mas sim porque problemas sempre irão surgir. Caso contrário, boa parte de nós estaríamos desempregados. Isso se deve ao fato de que sempre precisamos estar implementando novidades e nos adaptando às atualizações de outros softwares, navegadores e sistemas operacionais.  


## Sou naturalmente curioso e proativo

Gosto de estar a par de todo o processo. Isso facilita na identificação de algum problema e na resolução do mesmo. Isso me auxilia na aprendizagem e me deixa preparado para alguma eventual mudança/atualização.  


## Conclusão

Escrever é algo que aprendi aos 5 anos de idade (e aprimorei minha leitura/escrita lendo revistas em quadrinhos), logo, é muito fácil fazer uma pesquisa rápida e escrever o que as pessoas querem ou esperam ler... Outra coisa é demonstrar na prática! É justamente aí que normalmente me destaco: É demonstrando que realmente sei fazer tudo o que eu afirmei anteriormente e o quão longe posso ir. 💪  